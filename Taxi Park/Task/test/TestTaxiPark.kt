package taxipark

import org.junit.Assert
import org.junit.Test

class TestTaxiPark {
    @Test
    fun testFakeDrivers() {
        val tp = taxiPark(1..3, 1..2, trip(1, 1), trip(1, 2))
        Assert.assertEquals("Wrong result for 'findFakeDrivers()'." + tp.display(),
                drivers(2, 3).toSet(), tp.findFakeDrivers())
    }

    @Test
    fun testFaithfulPassengers() {
        val tp = taxiPark(1..3, 1..5, trip(1, 1), trip(2, 1), trip(1, 4), trip(3, 4), trip(1, 5), trip(2, 5), trip(2, 2))
        Assert.assertEquals("Wrong result for 'findFaithfulPassengers()'. MinTrips: 2." + tp.display(),
                passengers(1, 4, 5), tp.findFaithfulPassengers(2))
    }

    @Test
    fun testFrequentPassengers() {
        val tp = taxiPark(1..2, 1..4, trip(1, 1), trip(1, 1), trip(1, listOf(1, 3)), trip(1, 3), trip(1, 2), trip(2, 2))
        Assert.assertEquals("Wrong result for 'findFrequentPassengers()'. Driver: ${driver(1).name}." + tp.display(),
                passengers(1, 3), tp.findFrequentPassengers(driver(1)))
    }

    @Test
    fun testSmartPassengers() {
        val tp = taxiPark(1..2, 1..2, trip(1, 1, discount = 0.1), trip(2, 2))
        Assert.assertEquals("Wrong result for 'findSmartPassengers()'." + tp.display(),
                passengers(1), tp.findSmartPassengers())
    }

    @Test
    fun testTheMostFrequentTripDuration() {
        val tp = taxiPark(1..3, 1..5, trip(1, 1, duration = 10), trip(3, 4, duration = 30),
                trip(1, 2, duration = 20), trip(2, 3, duration = 35))
        // The period 30..39 is the most frequent since there are two trips (duration 30 and 35)
        Assert.assertEquals("Wrong result for 'findTheMostFrequentTripDurationPeriod()'.",
                30..39, tp.findTheMostFrequentTripDurationPeriod())
    }

    @Test
    fun testParetoPrincipleSucceeds() {
        val tp = taxiPark(1..5, 1..4,
                trip(1, 1, 20, 20.0),
                trip(1, 2, 20, 20.0),
                trip(1, 3, 20, 20.0),
                trip(1, 4, 20, 20.0),
                trip(2, 1, 20, 20.0))
        // The income of driver #1: 160.0;
        // the total income of drivers #2..5: 40.0.
        // The first driver constitutes exactly 20% of five drivers
        // and his relative income is 160.0 / 200.0 = 80%.

        Assert.assertEquals(
                "Wrong result for 'checkParetoPrinciple()'." + tp.display(),
                true, tp.checkParetoPrinciple())
    }

    @Test
    fun testParetoPrincipleSucceeds2() {
        val tp = taxiPark(0..9, 0..19,
                trip(6, 10, 16, 3.0),
                trip(1, 7, 28, 24.0, 0.4),
                trip(1, 13, 10, 19.0, 0.1),
                trip(3, 12, 4, 17.0, 0.2),
                trip(7, 8, 14, 22.0, 0.1),
                trip(8, 15, 22, 3.0, 0.4),
                trip(9, 2, 20, 0.0),
                trip(9, 14, 18, 1.0, 0.3),
                trip(4, 1, 2, 10.0),
                trip(7, 16, 24, 21.0, 0.1),
                trip(7, 9, 12, 25.0, 0.4),
                trip(1, 16, 22, 19.0),
                trip(7, 17, 15, 10.0),
                trip(6, 12, 4, 13.0, 0.3),
                trip(4, 14, 10, 7.0,0.3),
                trip(1, 2, 25, 1.0),
                trip(1, 6, 18, 19.0),
                trip(6, 15, 7, 5.0,0.3),
                trip(1, 18, 29, 28.0),
                trip(7, 14, 17, 25.0,0.3),
                trip(6, 10, 11, 19.0, 0.3),
                trip(7, 15, 22, 9.0),
                trip(7, 15, 12, 22.0),
                trip(8, 9, 1, 4.0),
                trip(7, 5, 19, 21.0, 0.3),
                trip(1, 12, 16, 12.0),
                trip(7, 13, 10, 17.0,0.2),
                trip(7, 19, 17, 28.0,0.4),
                trip(0, 10, 3, 5.0,0.3),
                trip(1, 6, 15, 11.0),
                trip(7, 19, 27, 29.0),
                trip(1, 12, 22, 21.0,0.1),
                trip(8, 11, 9, 0.0),
                trip(1, 5, 22, 28.0),
                trip(7, 1, 26, 14.0),
                trip(1, 7, 22, 15.0),
                trip(8, 14, 4, 8.0),
                trip(9, 12, 7, 0.0),
                trip(4, 1, 0, 15.0),
                trip(7, 4, 23, 21.0,0.2))

        Assert.assertEquals(
                "Wrong result for 'checkParetoPrinciple()'." + tp.display(),
                true, tp.checkParetoPrinciple())
    }

    @Test
    fun testParetoPrincipleFails() {
        val tp = taxiPark(1..5, 1..4,
                trip(1, 1, 20, 20.0),
                trip(1, 2, 20, 20.0),
                trip(1, 3, 20, 20.0),
                trip(2, 4, 20, 20.0),
                trip(3, 1, 20, 20.0))
        // The income of driver #1: 120.0;
        // the total income of drivers #2..5: 80.0.
        // The first driver constitutes 20% of five drivers
        // but his relative income is 120.0 / 200.0 = 60%
        // which is less than 80%.

        Assert.assertEquals(
                "Wrong result for 'checkParetoPrinciple()'." + tp.display(),
                false, tp.checkParetoPrinciple())
    }

    @Test
    fun testParetoPrincipleNoTrips() {
        val tp = taxiPark(1..5, 1..4)
        Assert.assertEquals(
                "Wrong result for 'checkParetoPrinciple()'." + tp.display(),
                false, tp.checkParetoPrinciple())
    }
}