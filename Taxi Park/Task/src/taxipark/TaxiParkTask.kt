package taxipark

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> =
        this.allDrivers
                .filter { d ->
                    this.trips.none { trip -> d.name.equals(trip.driver.name) }
                }
                .toSet()

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> =
        this.allPassengers
                .filter { passenger ->
                    this.trips
                            .filter { trip -> trip.passengers.contains(passenger) }
                            .count() >= minTrips
                }
                .toSet()

/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> =
        this.allPassengers
                .filter { passenger ->
                    this.trips
                            .filter { trip -> trip.passengers.contains(passenger) && driver.name.equals(trip.driver.name) }
                            .count() > 1
                }.toSet()

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> =
        this.allPassengers
                .filter { passenger ->
                    this.trips
                            .filter { trip -> trip.passengers.contains(passenger) && trip.discount != null }
                            .count() >
                            this.trips
                                    .filter { trip -> trip.passengers.contains(passenger) && trip.discount == null }
                                    .count()

                }.toSet()

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    if (this.trips.isEmpty()) {
        return null
    }

    val tripWithMaxDuration = trips.maxBy { trip -> trip.duration }
    val maxDuration = tripWithMaxDuration?.duration ?: 0

    val rangesWithNumberOfTrips = hashMapOf<IntRange, Int>()
    for (i in 0..maxDuration step 10) {
        val range = IntRange(i, i + 9)
        val numberOfResultsInRange = this.trips
                .filter { trip ->
                    trip.duration in range
                }.count()
        rangesWithNumberOfTrips[range] = numberOfResultsInRange
    }

    return rangesWithNumberOfTrips.maxBy { entry -> entry.value }?.key
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    if (this.trips.isEmpty()) {
        return false
    }
    val percentOfDrivers = (this.allDrivers.size * 0.2).toInt()

    val costDrivers = this.trips
            .groupBy { trip: Trip -> trip.driver }
            .map { entry: Map.Entry<Driver, List<Trip>> -> entry.value.sumByDouble { trip -> trip.cost } }
            .sorted()

    val topDriversCost = costDrivers.subList(costDrivers.size - percentOfDrivers, costDrivers.size).sum()

    val allCost = this.trips.sumByDouble { trip -> trip.cost }

    return topDriversCost.div(allCost) >= 0.8
}
