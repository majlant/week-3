package nicestring

fun String.isNice(): Boolean {
    val substring = listOf("bu", "ba", "be")

    val containsIllegalSubstring = substring.filter { s -> this.contains(s) }.count() > 0
    val containsVowels = this.count { c -> c.isEnglishVowel() } >= 3
    val containDoubleLetter = this.zipWithNext { a, b -> a == b }.count { b -> b } >= 1

    val results = listOf(!containsIllegalSubstring, containsVowels, containDoubleLetter)

    return results.count { b -> b } >= 2
}

private fun Char.isEnglishVowel(): Boolean {
    return this == 'a' || this == 'e' || this == 'i' || this == 'o' || this == 'u'
}